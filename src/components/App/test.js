import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';
import App, { updateSearchTopStoriesState } from './index';

describe('App', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<App />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  // https://reactjs.org/docs/test-renderer.html#ideas
  test('snapshots', () => {
    let focused = false;
    const component = renderer.create(<App />, {
      createNodeMock: el => {
        if (el.type === 'input') {
          return {
            focus: () => {
              focused = true;
            },
          };
        }
        return null;
      },
    });
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
    expect(focused).toBe(true);
  });
});
describe('updateSearchTopStoriesState', () => {
  const defaultState = {
    results: {},
    searchKey: '',
  };
  const oldHits = [{ a: 'hit0' }];
  const updatedState = {
    results: {
      key1: { hits: oldHits, page: 1 },
    },
    searchKey: 'key1',
  };
  const newHits = [{ a: 'hit1' }, { a: 'hit2' }];
  const newPage = 2;

  it('returns new hits, page, and isLoading by default', () => {
    const newState = updateSearchTopStoriesState(newHits, newPage)(
      defaultState,
    );
    expect(newState).toEqual({
      results: { '': { hits: newHits, page: 2 } },
      isLoading: false,
    });
  });
  it('returns old and new hits if old present', () => {
    const newState = updateSearchTopStoriesState(newHits, newPage)({
      ...defaultState,
      ...updatedState,
    });
    expect(newState.results.key1.hits).toEqual([...oldHits, ...newHits]);
  });
});
