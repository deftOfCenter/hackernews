import React, { Component } from 'react';
import './index.css';
import Table from '../Table';
import Search from '../Search';
import { ButtonWithLoading } from '../Button';
import {
  PATH_BASE,
  PATH_SEARCH,
  PARAM_SEARCH,
  PARAM_PAGE,
  PARAM_HPP,
  DEFAULT_QUERY,
  DEFAULT_HPP,
} from '../../constants';

// const url = `${PATH_BASE}${PATH_SEARCH}?${PARAM_SEARCH}${DEFAULT_QUERY}&${PARAM_PAGE}`;

const updateSearchTopStoriesState = (hits, page) => prevState => {
  const { results, searchKey } = prevState;
  const oldHits = results && results[searchKey] ? results[searchKey].hits : [];
  return {
    results: {
      ...results,
      [searchKey]: { hits: [...oldHits, ...hits], page },
    },
    isLoading: false,
  };
};

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      results: [],
      searchKey: '',
      searchTerm: DEFAULT_QUERY,
      error: null,
      isLoading: false,
    };

    this.setSearchTopstories = this.setSearchTopstories.bind(this);
    this.fetchSearchTopstories = this.fetchSearchTopstories.bind(this);
    this.onSearchChange = this.onSearchChange.bind(this);
    this.onSearchSubmit = this.onSearchSubmit.bind(this);
    this.onDismiss = this.onDismiss.bind(this);
  }

  componentDidMount() {
    this.setState(prevState => {
      const { searchTerm } = prevState;
      this.fetchSearchTopstories(searchTerm);
      return { searchKey: searchTerm };
    });
  }

  onDismiss(id) {
    const isNotId = item => item.objectID !== id;
    this.setState(prevState => {
      const { results, searchKey } = prevState;
      const { hits, page } = results[searchKey];
      const updatedHits = hits.filter(isNotId);
      return {
        results: {
          ...prevState.results,
          [searchKey]: { hits: updatedHits, page },
        },
      };
    });
  }

  onSearchChange(e) {
    this.setState({ searchTerm: e.target.value });
  }

  onSearchSubmit(e) {
    const { searchTerm } = this.state;
    this.setState({ searchKey: searchTerm });
    this.fetchSearchTopstories(searchTerm);
    e.preventDefault();
  }

  setSearchTopstories(result) {
    const { hits, page } = result;
    this.setState(updateSearchTopStoriesState(hits, page));
  }

  fetchSearchTopstories(searchTerm, page = 0) {
    this.setState({ isLoading: true });
    fetch(
      `${PATH_BASE}${PATH_SEARCH}?${PARAM_SEARCH}${searchTerm}&${PARAM_PAGE}${page}&${PARAM_HPP}${DEFAULT_HPP}`,
    )
      .then(response => response.json())
      .then(result => this.setSearchTopstories(result))
      .catch(e => this.setState({ error: e }));
  }

  render() {
    const { searchTerm, searchKey, results, error, isLoading } = this.state;
    const result = results && results[searchKey];
    const page = (result && result.page) || 0;
    return (
      <div className="page">
        <div className="interactions">
          <Search
            value={searchTerm}
            onChange={this.onSearchChange}
            onSubmit={this.onSearchSubmit}
          >
            Search
          </Search>
        </div>
        {error ? (
          <div className="interactions">
            <p>Something went wrong.</p>
          </div>
        ) : (
          result && (
            <Table list={result.hits || []} onDismiss={this.onDismiss} />
          )
        )}
        <div className="interaction">
          <ButtonWithLoading
            isLoading={isLoading}
            onClick={() => this.fetchSearchTopstories(searchKey, page + 1)}
          >
            More
          </ButtonWithLoading>
        </div>
      </div>
    );
  }
}
export default App;
export { updateSearchTopStoriesState };
