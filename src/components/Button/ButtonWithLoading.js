import React from 'react';
import PropTypes from 'prop-types';
import Button from './ButtonBase';

const withLoading = Component => {
  // eslint-disable-next-line arrow-body-style
  const wrapped = ({ isLoading, ...rest }) => {
    return isLoading ? <div>Loading</div> : <Component {...rest} />;
  };
  wrapped.propTypes = {
    isLoading: PropTypes.bool.isRequired,
  };
  return wrapped;
};

withLoading.propTypes = {
  Component: PropTypes.element,
};

const ButtonWithLoading = withLoading(Button);

export default ButtonWithLoading;
