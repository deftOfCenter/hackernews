import Button from './ButtonBase';
import ButtonWithLoading from './ButtonWithLoading';

export default Button;
export { ButtonWithLoading };
