import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Button from './index';

Enzyme.configure({ adapter: new Adapter() });

describe('Button', () => {
  const props = {
    onClick: () => {},
  };
  it('renders', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Button {...props}>Give Me More!</Button>, div);
  });

  test('snapshots', () => {
    const component = renderer.create(
      <Button {...props}>Give Me More!</Button>,
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
