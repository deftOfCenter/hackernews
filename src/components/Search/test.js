import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';
import Search from './index';

describe('Search', () => {
  const props = {
    onChange: () => {},
    onSubmit: () => {},
  };
  it('renders', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Search {...props}>Search</Search>, div);
  });

  // https://reactjs.org/docs/test-renderer.html#ideas
  test('snapshots', () => {
    let focused = false;
    const component = renderer.create(<Search {...props}>Search</Search>, {
      createNodeMock: el => {
        if (el.type === 'input') {
          return {
            focus: () => {
              focused = true;
            },
          };
        }
        return null;
      },
    });
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
    expect(focused).toBe(true);
  });
});
